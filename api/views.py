from rest_framework import viewsets
from api.models import ShortLink
from api.serializers import ShortLinkSerializer

# Create your views here.
class ShortLinkViewSet(viewsets.ModelViewSet):
    queryset = ShortLink.objects.all()
    serializer_class = ShortLinkSerializer
    lookup_field = "custom"
