from django.db import models
from uuid import uuid4
from django.utils.translation import gettext_lazy as _

# Create your models here.
class ShortLink(models.Model):
    id = models.TextField(default=uuid4, primary_key=True, help_text=_("Unique ID"))
    name = models.TextField(null=True, help_text=_("Name of short link"))
    origin = models.TextField(help_text=_("Original link"))
    custom = models.TextField(unique=True, help_text=_("Custom short link"))
